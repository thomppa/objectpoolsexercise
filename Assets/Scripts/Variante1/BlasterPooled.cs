﻿using System;
using UnityEngine;

public class BlasterPooled : MonoBehaviour
{
	[SerializeField] private float refireRate = 2;
	private float fireTimer;

	[SerializeField]
	private GameObjectPool gameObjectPool; //pool, der referenziert wird, anstelle eines einzigen existierenden Pools

	void Update()
	{
		fireTimer += Time.deltaTime; //simpler Timer
		if (fireTimer >= refireRate) //wenn RefireTimer erreicht
		{
			fireTimer = 0; //Timer wieder 0
			Fire();
		}
	}

	private void Fire()
	{
		var shot = gameObjectPool.Get();
		shot.transform.rotation = transform.rotation;
		shot.transform.position = transform.position;
		shot.gameObject.SetActive(true);
	}
}
