﻿using UnityEngine;
using Variante_4;

namespace Traps
{
    public class MultiShotTrapMono : MonoBehaviour
    {
        private MultiShotTrap _myTrap;

        [SerializeField] private float fireRate;
        [SerializeField] private float fireCounter;
        
        void Start()
        {
            _myTrap = this.gameObject.GetComponent<MultiShotTrap>();
        }

        void Update()
        {
            if (_myTrap.TrapActivated)
            {
                fireCounter -= Time.deltaTime;

                if (fireCounter <= 0)
                {
                    fireCounter = fireRate;
                    _myTrap.ActivateTrap();
                }
            }
        
        }
    }
}
