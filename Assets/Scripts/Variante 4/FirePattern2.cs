﻿using System;
using Traps;
using UnityEngine;

namespace Variante_4
{
    public class FirePattern2 : MonoBehaviour
    {
        private float angle = 0f;

        private void Start()
        {
            InvokeRepeating(nameof(Fire),0,.2f);
            
        }

        private void Fire()
        {
            float bulDirX = transform.position.x + Mathf.Sin((angle*Mathf.PI)/180f);
            float bulDirY = transform.position.x + Mathf.Cos((angle*Mathf.PI)/180f);
            
            Vector3 bulMoveVector = new Vector3(bulDirX,bulDirY,0f);
            Vector2 bulDir = (bulMoveVector - transform.position).normalized;

            GameObject bul = ProjectilePool.Instance.GetProjectile();
            bul.transform.position = transform.position;
            bul.transform.rotation = transform.rotation;
            bul.SetActive(true);
            bul.GetComponent<Projectile2>().SetMoveDirection(bulDir);

            angle += 60f;
        }
    }
}