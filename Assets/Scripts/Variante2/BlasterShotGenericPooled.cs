﻿using UnityEngine;

namespace Variante2
{
    public class BlasterShotGenericPooled : MonoBehaviour
    {
      public float moveSpeed;

      private float lifeTime;
      [SerializeField]
      private float maxLifeTime = 5f;
      
      
      private void OnEnable()
      {
          lifeTime = 0f;
      }

      private void Update()
      {
          transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
          lifeTime += Time.deltaTime;

          if(lifeTime >= maxLifeTime)
          {
              ShotPool.Instance.ReturnToPool(this);
          }
      }
      
    }
}
