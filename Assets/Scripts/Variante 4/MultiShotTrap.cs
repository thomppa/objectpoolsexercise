﻿using Traps;
using UnityEngine;

//Contributors: Kay
namespace Variante_4
{
    public class MultiShotTrap : Trap
    {
        
        [SerializeField] private Projectile2 myProjectile;
        [SerializeField] private Transform firePoint;

        [SerializeField] private int projectileAmount = 3;
        [SerializeField] private float startAngle = 90f;
        [SerializeField] private float endAngle = 270f;

        private void Fire()
        {
            float angleStep = (endAngle - startAngle) / projectileAmount;
            float angle = startAngle;

            for (int i = 0; i < projectileAmount; i++)
            {
                var position = firePoint.transform.position;
                
                float projDirX = position.x + Mathf.Sin((angle * Mathf.PI) / 180f);
                float projDirY = position.y + Mathf.Sin((angle * Mathf.PI) / 180f);
                
                Vector3 projMoveVector = new Vector3(projDirX,projDirY,0f);
                Vector2 projDir = (projMoveVector - transform.position).normalized;


                GameObject p = ProjectilePool.Instance.GetProjectile();
                p.transform.position = position;
                p.transform.rotation = transform.rotation;
                p.gameObject.SetActive(true);
                p.GetComponent<Projectile2>().direction = projDir;

                angle += angleStep;
            }
        }
        
        public override void ActivateTrap()
        {
            Fire();
        }

        public override void DeactivateTrap()
        {
            TrapActivated = false;
        }
    }
}
