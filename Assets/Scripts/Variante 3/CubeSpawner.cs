﻿using System;
using UnityEngine;

namespace Variante_3
{
    public class CubeSpawner : MonoBehaviour
    {
        private ObjectPoolerBrackeys _objectPoolerBrackeys;

        private void Start()
        {
           _objectPoolerBrackeys = ObjectPoolerBrackeys.Instance; 
        }

        private void FixedUpdate()
        {
            _objectPoolerBrackeys.SpawnFromPool("Cube", transform.position, Quaternion.identity);

        }
    }
}