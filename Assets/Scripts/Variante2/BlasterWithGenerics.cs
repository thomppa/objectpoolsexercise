﻿using UnityEngine;

namespace Variante2
{
	public class BlasterWithGenerics : MonoBehaviour
	{
		[SerializeField] private float refireRate = 2;
		
		private float fireTimer;

		void Update()
		{
			fireTimer += Time.deltaTime; //simpler Timer
			if (fireTimer >= refireRate) //wenn RefireTimer erreicht
			{
				fireTimer = 0; //Timer wieder 0
				Fire();
			}
		}

		private void Fire()
		{
			var shot = ShotPool.Instance.Get();
			shot.transform.rotation = transform.rotation;
			shot.transform.position = transform.position;
			shot.gameObject.SetActive(true);
		}
	}
}
