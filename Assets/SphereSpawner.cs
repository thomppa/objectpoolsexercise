﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
        private ObjectPoolerBrackeys _objectPoolerBrackeys;

        private void Start()
        {
           _objectPoolerBrackeys = ObjectPoolerBrackeys.Instance; 
        }

        private void FixedUpdate()
        {
            _objectPoolerBrackeys.SpawnFromPool("Sphere", transform.position, Quaternion.identity);

        }
}
