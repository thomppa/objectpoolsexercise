﻿using System;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.WSA;

//CONTRIBUTORS: Kay
namespace Traps
{
    [RequireComponent(typeof(Animator))]
    public abstract class Trap : MonoBehaviour
    {
        #region Public Properties
        
            public Animator MyAnim { get; set; }
            
            public bool TrapActivated { get; set; }

        #endregion

        
        private void Awake()
        {
            this.tag = "Trap";

            MyAnim = GetComponent<Animator>();
        }

        public abstract void ActivateTrap();
        public abstract void DeactivateTrap();
    }
}
