﻿using UnityEngine;

//Contributors: Kay
namespace Variante_4
{
    public class Projectile2 : MonoBehaviour
    {
        [SerializeField] private float projectileSpeed;

        public Vector2 direction;
        void Start()
        {
        }

        private void Update()
        {
            transform.Translate(direction * projectileSpeed * Time.deltaTime);
        }

        public void SetMoveDirection(Vector3 dir)
        {
            direction = dir;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                DestroyProjectile();
            }
        }

        private void OnEnable()
        {
            Invoke(nameof(DestroyProjectile), 5f);
        }

        private void OnBecameInvisible()
        {
            DestroyProjectile();
        }

        private void DestroyProjectile()
        {
            gameObject.SetActive(false);
        }
    }
}
