﻿namespace Variante_3
{
    public interface IPooledObject
    { 
        void OnObjectSpawn();
    }
}