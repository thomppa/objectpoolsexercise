﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Variante_3
{
    public class Cube : MonoBehaviour, IPooledObject
    {
        public float upforce = 1f;
        public float sideForce = .1f;

        public void OnObjectSpawn()
        {
            float xForce = Random.Range(-sideForce, sideForce);
            float yForce = Random.Range(upforce /2f ,upforce);
            float zForce = Random.Range(-sideForce, sideForce);
            
            Vector3 force = new Vector3(xForce,yForce,zForce);

            GetComponent<Rigidbody>().velocity = force;
            
        }
    }
}