﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Variante2
{
    public abstract class GenericObjectPool<TypeToPool> : MonoBehaviour where TypeToPool : Component //TypeToPool MUST be a component
    {
        [SerializeField] private TypeToPool prefab;

        public static GenericObjectPool<TypeToPool> Instance { get; private set; }
        
        private Queue<TypeToPool> objects = new Queue<TypeToPool>();

        private void Awake()
        {
            Instance = this;
        }

        public TypeToPool Get()
        {
            if (objects.Count == 0)
            {
                AddObjects(1);
            }

            return objects.Dequeue();
        }

        public void ReturnToPool(TypeToPool objectToReturn)
        {
            objectToReturn.gameObject.SetActive(false);
            objects.Enqueue(objectToReturn);
        }

        private void AddObjects(int count)
        {
            var newObject = GameObject.Instantiate(prefab);
            newObject.gameObject.SetActive(false);
            objects.Enqueue(newObject);
        }
        
        
    }
}
