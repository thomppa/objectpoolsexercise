﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Contributor: Kay
namespace Traps
{
    public class ProjectilePool : MonoBehaviour
    {
        public static ProjectilePool Instance;

        [SerializeField] private GameObject pooledProjectile;

        private List<GameObject> _projectiles;
        
        private bool _insufficientProjectiles = true;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
        }

        private void Start()
        {
           _projectiles = new List<GameObject>();
        }

        public GameObject GetProjectile()
        {
            if (_projectiles.Count > 0)
            {
                for (int p = 0; p < _projectiles.Count; p++)
                {
                    if (!_projectiles[p].activeInHierarchy)
                    {
                        return _projectiles[p];
                    }
                }
            }

            if (_insufficientProjectiles)
            {
                GameObject p = Instantiate(pooledProjectile);
                p.gameObject.SetActive(false);
                _projectiles.Add(p);
                return p;
            }

            return null;
        }
    }
}
