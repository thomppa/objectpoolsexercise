  using System;
  using System.Collections.Generic;
  using UnityEngine;

  public class GameObjectPool : MonoBehaviour
  {
      [SerializeField] private GameObject prefab;
      
      private Queue<GameObject> objects = new Queue<GameObject>(); //First in, First Out != Stack: Last in, First out
      public static GameObjectPool Instance { get; private set; } //private, so that nothing else can set the instance

      private void Awake()
      {
          Instance = this;
      }

      public GameObject Get()
      {
          if (objects.Count == 0)
          {
              AddObjects(1);
          }

          return objects.Dequeue();
      }

      public void ReturnToPool(GameObject objectToReturn)
      {
          objectToReturn.SetActive(false);
          objects.Enqueue(objectToReturn);
      }

      private void AddObjects(int count)
      {
          var newObject = GameObject.Instantiate(prefab);
          newObject.SetActive(false);
          objects.Enqueue(newObject);
          newObject.GetComponent<IGameObjectPooled>().Pool = this;
      }
  }

